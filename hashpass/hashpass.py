import random
import hashlib
import hmac
import getpass

DEFAULT_PASS_LENGTH = 16

class HashPass(object):
    def __init__(self, length=DEFAULT_PASS_LENGTH):
        self.passwords = {}
        self.defaultLength = length

    @staticmethod
    def getMasterPassHash():
        return hashlib.sha512(getpass.getpass("Please enter master password:")).hexdigest()

    def addPassword(self, name, length=None):
        if length is None:
            self.passwords[name] = [1, self.defaultLength]
        else:
            self.passwords[name] = [1, length]

    def getPassword(self, name, length=None, jumpahead=1):
        masterhash = self.getMasterPassHash()
        random.seed(hmac.new(name, masterhash, hashlib.sha512).hexdigest())
        if name in self.passwords:
            random.jumpahead(self.passwords[name][0])
            length = self.passwords[name][1]
        else:
            random.jumpahead(jumpahead)
            length = self.defaultLength
        return hmac.new(str(random.random()), name, hashlib.sha512).hexdigest()[:length]

    def updatePasswordLength(self, name, length):
        self.passwords[name][1] = length

    def nextPassword(self, name):
        self.passwords[name][0] += 1

